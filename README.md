# GamesStore-eCommerce-SpringBoot-Hib-MySQL
A Humble Bundle clone. 

Goals:
Front-end: Thymeleaf + Bootstrap 
Back-end: Spring Boot + Spring Data + Spring Security + Hibernate + MySQL

Features:
CRUD admin pages for every main entities.
A dynamic bundle page.
A dynamic store page.
User Registration.
Remember me.
Shopping cart.
Order history.
Validation.
Authentication and authorization.

Status: Finished.

Live site: http://www.saturnringstation.com/humblebundle
